from django import forms

class ContactsForm(forms.Form):
    name = forms.CharField(max_length=30, widget=forms.TextInput(attrs=({'placeholder': 'Имя', 'class': 'request_smth'})))
    service = forms.CharField(max_length=50, widget=forms.TextInput(attrs=({'placeholder': 'Услуга', 'class': 'request_smth1'})))
    email = forms.EmailField(widget=forms.EmailInput(attrs=({'placeholder': 'Почта', 'class': 'request_smth1'})))
    message = forms.CharField(widget=forms.Textarea(attrs=({'placeholder': 'Комментарий', 'class': 'request_smth1--comments'})))


