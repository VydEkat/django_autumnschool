from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect, JsonResponse
from .models import *
from .forms import ContactsForm
# from django.views.generic import TemplateView

def index (request):
    success = 0
    if request.method == 'POST':
        form = ContactsForm(request.POST)
        if form.is_valid():
            data = Contacts(
                name=form.cleaned_data['name'],
                service=form.cleaned_data['service'],
                email=form.cleaned_data['email'],
                message=form.cleaned_data['message'],)
            success = 1
            data.save()
    else:
        form = ContactsForm()

    services = Services.objects.all()
    form = ContactsForm()
    data = {
        'services': services,
        'form': form,
        'success': success,
    }
    return render(request, 'content/index.html', data)

# def index(request):
#     return HttpResponse("Страница приложения home.")

def categories(request, catid):
    return HttpResponse(f"<h1>Статьи по категориям</h1><p>{catid}</p>")

def pageNotFound(request, exception):
    return HttpResponseNotFound('<h1>Страница не найдена</h1>')

# class Home(TemplateView):
#     template_name = 'index.html'
#
#     def get(self, request):
#         ctx = {}
#         return render(request, self.template_name, ctx)

def contacts(request):
    form = ContactsForm()
    return render(request, 'content/index.html', {'form': form})


def test_view(request):
    return HttpResponse("welcome to %s" %request.path)
