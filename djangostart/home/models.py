from django.db import models



class Contacts(models.Model):
        name = models.CharField('Имя', max_length=50)
        service = models.CharField('Услуга', max_length=50)
        email = models.EmailField('Почта')
        message = models.TextField('Комментарий')

        def __str__(self):
            # Будет отображаться следующее поле в панели администрирования
            return self.email

class Services(models.Model):
    title = models.CharField('Название', max_length=50)
    description = models.TextField('Описание')
    cost = models.IntegerField('Цена')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Service'
        verbose_name_plural = 'Services'

