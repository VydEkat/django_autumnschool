from django.urls import path

from . import views
from .views import *

app_name = 'home'

urlpatterns = [
    # path('', Home.as_view()),
    path('cats/<int:catid>/', categories),
    path('', index, name='index'),
    # path('', contacts, name='contacts'),
    path('test_view/', test_view)


]
